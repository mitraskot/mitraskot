# Load Image
# image will be loaded to a 2d array (3d array)
from PIL import Image,ImageOps
from random import randrange

# Convert image to Binary Image
def imageBinayConvert(img):
    original_img_pixels = img.load()  

    for row in range(img.size[1]):
        for col in range(img.size[0]):     
            if original_img_pixels[col,row] >= 128:
                original_img_pixels[col,row] = ((255))
            else:
                original_img_pixels[col,row] = ((0))
    return

#Load image file
def loadImage(path):
    return Image.open(path) 

#Convert Image to gray sacle
def toGrayScale(img):
    img = ImageOps.grayscale(original_img)
    return img.convert('L')

# Generate to random image (black and white)
def createRandomImages(Mode,witdh,height):
    Rand1 = Image.new(mode=Mode, size=(int(witdh),int(height)))    
    Rand2 = Image.new(mode=Mode, size=(int(witdh),int(height)))
    rand1_pixels = Rand1.load()        # create the pixel map
    rand2_pixels = Rand2.load()        # create the pixel map
    for row in range(Rand1.height):        # for every col:
        for col in range(Rand1.width):     # For every row
            value = randrange(255)
            rand1_pixels[col,row] = ((value))
            value = randrange(255)
            rand2_pixels[col,row] = ((value))
    return (Rand1,Rand2)
# Encypy the image.
def encrypt(Img,Rand1,Rand2):
    Img_pixels = Img.load()  
    rand1_pixels = Rand1.load()       
    rand2_pixels = Rand2.load()        
    for row in range(Rand1.height):        # for every col:
        for col in range(Rand1.width):     # For every row
            if  Img_pixels[col,row] == 0:
                rand2_pixels[col,row] = Img_pixels[col,row] ^ rand1_pixels[col,row]
    return
# Flip image vertically Vertical
def flipImageVertical(img):
    return ImageOps.mirror(img)


original_img = loadImage("./Mitraskot.png")
original_img  = toGrayScale(original_img)
imageBinayConvert(original_img)
Rand1,Rand2 = createRandomImages("L",original_img.width,original_img.height)
encrypt(original_img, Rand1,Rand2)
Rand2 = flipImageVertical(Rand2)

# Save image as file
Rand1.save('./First.png', quality=95)
Rand2.save('./Second.png', quality=95)


